#include <string>
#include "BioEnemyShip.h"


BioEnemyShip::BioEnemyShip()
{
	SetSpeed(150);
	SetMaxHitPoints(3);
	SetCollisionRadius(20);
}


void BioEnemyShip::Update(const GameTime *pGameTime)
{
	if (IsActive())
	{
		float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex());
		x *= GetSpeed() * pGameTime->GetTimeElapsed() * 1.4f;
		TranslatePosition(x, GetSpeed() * pGameTime->GetTimeElapsed());
		
		

		
	}

	EnemyShip::Update(pGameTime);
}


void BioEnemyShip::Draw(SpriteBatch* pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::Red, m_pTexture->GetCenter(), Vector2::ONE, Math::PI, 1);
		float HP = GetHitPoints();
		if (HP < 3)
		{
			pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::Blue, m_pTexture->GetCenter(), Vector2::ONE, Math::PI, 1);
		}
		if (HP < 2)
		{
			pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::Green, m_pTexture->GetCenter(), Vector2::ONE, Math::PI, 1);
		}
	}
}
