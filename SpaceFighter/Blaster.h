
#pragma once

#include "Weapon.h"

class Blaster : public Weapon
{

public:

	Blaster(const bool isActive) : Weapon(isActive)
	{
		m_cooldown = 0;

		//changed from 0.35 in gustavson version
		m_cooldownSeconds = 0.05;
	}

	virtual ~Blaster() { }

	virtual void Update(const GameTime *pGameTime)
	{
		if (m_cooldown > 0) m_cooldown -= pGameTime->GetTimeElapsed();
	}

	virtual bool CanFire() const { return m_cooldown <= 0; }

	virtual void ResetCooldown() { m_cooldown = 0; }

	virtual float GetCooldownSeconds() { return m_cooldownSeconds; }

	virtual void SetCooldownSeconds(const float seconds) { m_cooldownSeconds = seconds; }

	virtual void Fire(TriggerType triggerType)
	{
		//checks if the weapon is firing and is able too as well
		if (IsActive() && CanFire())
		{
			//checks if the triggertype contains a regular normal or special gun type
			if (triggerType.Contains(GetTriggerType()))
			{
				//creates a projectile pointer
				Projectile *pProjectile = GetProjectile();
				//checks if the projectile still exist
				if (pProjectile)
				{
					// activates the projectile and makes it do damage
					pProjectile->Activate(GetPosition(), true);
					//sets the cooldown to whatever the m_cooldownSeconds is;
					m_cooldown = m_cooldownSeconds;
				}
			}
		}
	}


private:

	float m_cooldown;
	float m_cooldownSeconds;

};