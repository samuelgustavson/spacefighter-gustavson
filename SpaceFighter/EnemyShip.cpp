
#include "EnemyShip.h"


EnemyShip::EnemyShip()
{
	SetMaxHitPoints(5);
	SetCollisionRadius(20);
}


void EnemyShip::Update(const GameTime* pGameTime)
{
	//checks if the delay counter is above zero
	if (m_delaySeconds > 0)
	{
		//removes elapsed seconds from delay seconds
		m_delaySeconds -= pGameTime->GetTimeElapsed();

		//activates the game object once the timer hits zero by subtracting elapsed time
		if (m_delaySeconds <= 0)
		{
			GameObject::Activate();
		}
	}
	//checks if there is an enemy already active
	if (IsActive())
	{
		
		//accumulates elapsed time into the activation variable
		m_activationSeconds += pGameTime->GetTimeElapsed();
		//removes enemies object if its off the screen and has been alive for more than 2 seconds)
		if (m_activationSeconds > 2 && !IsOnScreen()) Deactivate();
	}

	//runs the weapon if then statement for firing weapons
	Ship::Update(pGameTime);

}


void EnemyShip::Initialize(const Vector2 position, const double delaySeconds)
{
	SetPosition(position);
	m_delaySeconds = delaySeconds;

	Ship::Initialize();
}


void EnemyShip::Hit(const float damage)
{
	Ship::Hit(damage);
	
}