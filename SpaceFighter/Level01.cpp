

#include "Level01.h"
#include "BioEnemyShip.h"
#include "RyanEnemyShip.h"


void Level01::LoadContent(ResourceManager *pResourceManager)
{
	

	// Setup enemy ships
	Texture *pTexture = pResourceManager->Load<Texture>("Textures\\BioEnemyShip.png");
	Texture *rTexture = pResourceManager->Load<Texture>("Textures\\RyanEnemyShip.png");

	const int COUNT = 25;

	double xPositions[COUNT] =
	{
		0.25, 0.3, 0.4,
		0.5, 0.8, 0.7,
		0.3, 0.25, 0.35, 0.2, 0.4,
		0.7, 0.75, 0.65, 0.8, 0.6,
		0.5, 0.4, 0.6, 0.45, 0.55, 0.66, 0.70, 0.80, 0.9
	};
	
	double delays[COUNT] =
	{
		0.0, 0.25, 0.25,
		3.0, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.5, 0.3, 0.3, 0.3, 0.3, 0.4, 0.4, 0.4, 0.4
	};

	double delays2[]
	{
		5, 9, 12, 15, 18
	};


	//adjusted the speed of the spawn time for bio enemies
	float delay = 1.0; // start delay
	float delay2 = 1.0;
	Vector2 position;

	for (int i = 0; i < COUNT; i++)
	{
		delay += delays[i];
		position.Set(xPositions[i] * Game::GetScreenWidth(), -pTexture->GetCenter().Y);

		BioEnemyShip *pEnemy = new BioEnemyShip();
		pEnemy->SetTexture(pTexture);
		pEnemy->SetCurrentLevel(this);
		pEnemy->Initialize(position, (float)delay);
		AddGameObject(pEnemy);

	}

	
	for (int i = 0; i < 5; i++)
	{
		delay2 += delays2[i];
		position.Set(Game::GetScreenWidth() - Game::GetScreenWidth(), Game::GetScreenHeight() * xPositions[i]);

		RyanEnemyShip* rEnemy = new RyanEnemyShip();
		rEnemy->SetTexture(rTexture);
		rEnemy->SetCurrentLevel(this);
		rEnemy->Initialize(position, (float)delay2);
		rEnemy->Fire();
		AddGameObject(rEnemy);
		
	}


	Level::LoadContent(pResourceManager);
}

