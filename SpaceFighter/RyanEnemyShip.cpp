#include <string>
#include "RyanEnemyShip.h"



RyanEnemyShip::RyanEnemyShip()
{
	SetSpeed(50);
	SetMaxHitPoints(10);
	SetCollisionRadius(20);

	
}


void RyanEnemyShip::Update(const GameTime* pGameTime)
{
	if (IsActive())
	{
		float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex());
		x *= GetSpeed() * pGameTime->GetTimeElapsed() * 1.4f;
		TranslatePosition(GetSpeed() * pGameTime->GetTimeElapsed(), x);
		if (2 > 1) FireWeapons(TriggerType::PRIMARY);
	}

	EnemyShip::Update(pGameTime);
}

void RyanEnemyShip::Draw(SpriteBatch* pSpriteBatch)
{
	int HP = GetHitPoints();

	

	if(IsActive())
	{
 		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter(), Vector2::ONE, Math::PI, 1);
		if (HP != m_hitCount)
		{
			pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::Red, m_pTexture->GetCenter(), Vector2::ONE, Math::PI, 1);
			m_hitCount--;
		}
		else
		{
			pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter(), Vector2::ONE, Math::PI, 1);
		}

	}
}