
#pragma once

#include "EnemyShip.h"

class RyanEnemyShip : public EnemyShip
{

public:

	RyanEnemyShip();
	virtual ~RyanEnemyShip() { }

	void SetTexture(Texture* pTexture) { m_pTexture = pTexture; }

	virtual void Update(const GameTime* pGameTime);

	virtual void Draw(SpriteBatch* pSpriteBatch);

	int m_hitCount = 10;


private:

	Texture* m_pTexture;

};